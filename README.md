
# Setup Aplikasi

## Untuk jalan di lokal


1. Buat virtual environment

```bash
python -m venv venv
```
2. Run venv  
```bash
venv\Scripts\activate
```
3. Install requirements

```bash
pip install -r requirements.txt
```

4. Python main.py

link api nya
- '/get/topwords'
- '/get/popular/users'
- '/get/popular/mentions'
- '/get/popular/hourly'

