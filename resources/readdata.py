from flask import request, jsonify
from flask_restful import Resource
import json
import collections
import datetime



class GetTopWord(Resource):
	def get(self):

		with open('datasetfinal.json', encoding='utf-8-sig') as json_file:

			text = json_file.read()
			json_data = json.loads(text)
		
		# Stopwords
		stopwords = set(line.strip() for line in open('stopwords.txt'))
		gabung = " "

		for i in json_data: 
			gabung += str(i['text']) + " "
		
		totalkata = {}
		for word in gabung.lower().split():
			word = word.replace(".","")
			word = word.replace(",","")
			word = word.replace(":","")
			word = word.replace("\"","")
			word = word.replace("!","")
			word = word.replace("â€œ","")
			word = word.replace("â€˜","")
			word = word.replace("*","")
			if word not in stopwords:
				if word not in totalkata:
					totalkata[word] = 1
				else:
					totalkata[word] += 1

		
		banyaklist = int(10)
		word_counter = collections.Counter(totalkata)
		resp = {}
		for word, count in word_counter.most_common(banyaklist):
			resp['{}'.format(word)] = count
			

		return resp, 200

class GetFromName(Resource):
	def get(self):
		
		with open('datasetfinal.json', encoding='utf-8-sig') as json_file:

			text = json_file.read()
			json_data = json.loads(text)
		
		# Stopwords
		gabung = " "

		for i in json_data: 
			gabung += str(i['fromuser']) + " "
		
		totalkata = {}
		for word in gabung.lower().split():
			if word not in totalkata:
				totalkata[word] = 1
			else:
				totalkata[word] += 1

		
		banyaklist = int(10)
		word_counter = collections.Counter(totalkata)
		resp = {}
		for word, count in word_counter.most_common(banyaklist):
			resp['{}'.format(word)] = count
			

		return resp, 200

class GetMentions(Resource):
	def get(self):

		with open('datasetfinal.json', encoding='utf-8-sig') as json_file:

			text = json_file.read()
			json_data = json.loads(text)
		
		# # Stopwords
		gabung = " "

		for i in json_data: 
			try:
				for j in i['mentions']:
					gabung += str(i['mentions']) + " "

			except:
				print("-")
		totalkata = {}
		for word in gabung.lower().split():
			word = word.replace("[","")
			word = word.replace(",","")
			word = word.replace("]","")
			word = word.replace("'","")
			if word not in totalkata:
				totalkata[word] = 1
			else:
				totalkata[word] += 1

		
		banyaklist = int(10)
		word_counter = collections.Counter(totalkata)
		resp = {}
		for word, count in word_counter.most_common(banyaklist):
			resp['{}'.format(word)] = count
			

		return resp, 200


class Gethourly(Resource):
	def get(self):

		with open('datasetfinal.json', encoding='utf-8-sig') as json_file:

			text = json_file.read()
			json_data = json.loads(text)
		
		jams = []
		for i in json_data: 
			timestamp = datetime.datetime.fromtimestamp(int(i['createdat']))
			jams.append(timestamp.strftime('%H'))
			
		
		result = dict(collections.Counter(jams))
		resp = collections.OrderedDict(sorted(result.items()))
		
			

		return resp, 200