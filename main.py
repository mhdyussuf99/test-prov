import os
from flask import Flask
from flask_restful import Api
from resources.readdata import *

#isi Importnya 

app = Flask(__name__)


api = Api(app)

#IsiAPI

api.add_resource(GetTopWord, '/get/topwords')
api.add_resource(GetFromName, '/get/popular/users')
api.add_resource(GetMentions, '/get/popular/mentions')
api.add_resource(Gethourly, '/get/popular/hourly')


@app.route('/')
def index():
    return "Environment saat ini: local"

if __name__ == "__main__":
    
    app.run(debug=True)